package question1;

public class Circle implements Shape {

    private double area;

    public Circle(double radius) {
        area = Math.PI * radius * radius;
    } 

    @Override
    public double area() {
        return area;
    }

    @Override
    public String getName() {
        return "Circle";
    }
    
}