package question2;

import java.util.ArrayList;

public class MammalNoise {
    
    public static void main(String[] args) {
        ArrayList<Mammal> mammals = new ArrayList<>();
        mammals.add(new Dog());
        mammals.add(new Cat());

        for (Mammal m : mammals) {
            if (m instanceof Dog) {
                System.out.println("Dog noise is: " + m.makeNoise());
            }
            else if (m instanceof Cat) {
                System.out.println("Cat noise is: " + m.makeNoise());
            }
            else {
                System.out.println("Mammal noise is: " + m.makeNoise());
            }
        }

        
    }

}