package question2;

import java.util.ArrayList;

public class AnyMammalNoise {

    public static void main(String[] args) {
        ArrayList<Mammal> mammals = new ArrayList<>();
        mammals.add(new Dog());
        mammals.add(new Cat());
        mammals.add(new Cow());

        for (Mammal m : mammals) {
            System.out.println("Mammal noise is: " + m.makeNoise());
        }
        
    }
}